/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.AuthorizationVo;
import cn.easyplatform.messages.vos.EnvVo;
import cn.easyplatform.messages.vos.OrgVo;
import cn.easyplatform.spi.service.IdentityService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.contexts.Contexts;
import cn.easyplatform.web.service.ServiceLocator;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.A;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Window;

import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SelectOrgDlg extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private List<OrgVo> orgs;

	public SelectOrgDlg(List<OrgVo> orgs) {
		this.orgs = orgs;
		createContent();
	}

	private void createContent() {
		Listener listener = new Listener();
		Div div = new Div();
		div.setClass("list-group");
		for (int i = 0; i < orgs.size(); i++) {
			OrgVo org = orgs.get(i);
			A a = new A();
			a.setLabel(org.getName());
			a.setAttribute("value", org);
			if (i == 0)
				a.setClass("list-group-item active");
			else
				a.setClass("list-group-item");
			a.setParent(div);
			a.addEventListener(Events.ON_CLICK, listener);
		}
		div.setParent(this);
		Div buttons = new Div();
		buttons.setStyle("margin-top:-10px;padding-bottom:5px");
		Button button = new Button(Labels.getLabel("button.ok"));
		button.setMold("bs");
		button.setWidth("100px");
		button.setClass("btn-primary");
		button.addEventListener(Events.ON_CLICK, listener);
		button.setParent(buttons);
		button = new Button(Labels.getLabel("button.cancel"));
		button.setMold("bs");
		button.setWidth("100px");
		button.setClass("btn-default");
		button.setStyle("margin-left:5px");
		button.addForward(Events.ON_CLICK, this, Events.ON_CLOSE);
		button.setParent(buttons);
		buttons.setParent(this);

		setMaximizable(false);
		setMinimizable(false);
		setClosable(false);
		setWidth("450px");
		// setBorder(true);
		setSclass("text-center");
		// setTitle(Labels.getLabel("user.select.org.title"));
		// Caption cap = new Caption();
		// cap.setLabel(Labels.getLabel("user.select.org.title"));
		// cap.setImage("/images/organisation.png");
		// cap.setParent(this);
		addEventListener(Events.ON_CLOSE, listener);
	}

	private class Listener implements EventListener<Event> {
		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.zkoss.web.ui.event.EventListener#onEvent(org.zkoss.web.ui.event
		 * .Event)
		 */
		@Override
		public void onEvent(Event event) throws Exception {
			if (event.getTarget() instanceof A) {
				for (Component comp : event.getTarget().getParent()
						.getChildren()) {
					if (comp == event.getTarget())
						((A) comp).setClass("list-group-item active");
					else
						((A) comp).setClass("list-group-item");
				}
			} else if (event.getName().equals(Events.ON_CLICK)) {
				OrgVo org = null;
				for (Component comp : event.getTarget().getParent()
						.getPreviousSibling().getChildren()) {
					if (((A) comp).getSclass().equals("list-group-item active")) {
						org = (OrgVo) comp.getAttribute("value");
						break;
					}
				}
				IdentityService uc = ServiceLocator
						.lookup(IdentityService.class);
				SimpleRequestMessage req = new SimpleRequestMessage(org.getId());
				IResponseMessage<?> resp = uc.selectOrg(req);
				if (resp.isSuccess()) {
					EnvVo env = (EnvVo) Contexts.getEnv();
					AuthorizationVo av = (AuthorizationVo) resp.getBody();
					env.setMainPage(av.getMainPage());
					Session session = event.getTarget().getDesktop().getSession();
					session.setAttribute(Contexts.PLATFORM_USER_AUTHORIZATION,
							resp.getBody());
					session.setAttribute(Contexts.PLATFORM_USER_ORGS, orgs);
					Executions.sendRedirect("main.go");
				}
			} else {
				IdentityService uc = ServiceLocator
						.lookup(IdentityService.class);
				uc.logout(new SimpleRequestMessage());
				uc.getAppEnv(new SimpleRequestMessage(Contexts.getEnv()));
			}
		}
	}
}
