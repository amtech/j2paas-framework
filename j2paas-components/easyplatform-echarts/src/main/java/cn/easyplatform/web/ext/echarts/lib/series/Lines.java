/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.series.base.Effect;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Lines extends Series {
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Integer geoIndex;
    private Boolean polyline;
    private Effect effect;
    private Boolean large;
    private Integer largeThreshold;
    private Object symbol;
    private Object symbolSize;
    private LineStyle lineStyle;
    private Object progressiveThreshold;
    private Object progressive;

    public Lines() {
        type = "lines";
    }

    public Object coordinateSystem() {
        return coordinateSystem;
    }

    public Lines coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Object progressiveThreshold() {
        return progressiveThreshold;
    }

    public Lines progressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
        return this;
    }

    public Object progressive() {
        return xAxisIndex;
    }

    public Lines progressive(Object progressive) {
        this.progressive = progressive;
        return this;
    }

    public Object xAxisIndex() {
        return xAxisIndex;
    }

    public Lines xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Object yAxisIndex() {
        return yAxisIndex;
    }

    public Lines yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Object geoIndex() {
        return geoIndex;
    }

    public Lines geoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
        return this;
    }

    public Object polyline() {
        return polyline;
    }

    public Lines polyline(Boolean polyline) {
        this.polyline = polyline;
        return this;
    }

    public Object effect() {
        if (effect == null)
            effect = new Effect();
        return effect;
    }

    public Lines effect(Effect effect) {
        this.effect = effect;
        return this;
    }

    public Object large() {
        return large;
    }

    public Lines large(Boolean large) {
        this.large = large;
        return this;
    }

    public Object largeThreshold() {
        return largeThreshold;
    }

    public Lines largeThreshold(Integer largeThreshold) {
        this.largeThreshold = largeThreshold;
        return this;
    }

    public Object symbol() {
        return symbol;
    }

    public Lines symbol(Object symbol) {
            this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return symbolSize;
    }

    public Lines symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public LineStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return this.lineStyle;
    }

    public Series lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Integer getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
    }

    public Boolean getPolyline() {
        return polyline;
    }

    public void setPolyline(Boolean polyline) {
        this.polyline = polyline;
    }

    public Effect getEffect() {
        return effect;
    }

    public void setEffect(Effect effect) {
        this.effect = effect;
    }

    public Boolean getLarge() {
        return large;
    }

    public void setLarge(Boolean large) {
        this.large = large;
    }

    public Integer getLargeThreshold() {
        return largeThreshold;
    }

    public void setLargeThreshold(Integer largeThreshold) {
        this.largeThreshold = largeThreshold;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public Object getProgressiveThreshold() {
        return progressiveThreshold;
    }

    public void setProgressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
    }

    public Object getProgressive() {
        return progressive;
    }

    public void setProgressive(Object progressive) {
        this.progressive = progressive;
    }
}
