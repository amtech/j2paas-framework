/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.application;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.GetValueRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.GetValueVo;
import cn.easyplatform.messages.vos.datalist.ListGetValueVo;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GetValueCmd extends AbstractCommand<GetValueRequestMessage> {

    /**
     * @param req
     */
    public GetValueCmd(GetValueRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        if (cc.getWorkflowContext() == null || cc.getWorkflowContext().getRecord() == null)
            return new SimpleResponseMessage();
        GetValueVo gvv = req.getBody();
        if (gvv instanceof ListGetValueVo) {
            ListGetValueVo lgvv = (ListGetValueVo) gvv;
            ListContext lc = cc.getWorkflowContext().getList(lgvv.getId());
            RecordContext rc = lc.getRecord(lgvv.getKeys());
            if (rc == null) {
                if (lc.isCustom()) {
                    FieldDo[] data = DataListUtils.getRow(cc, lc,
                            lgvv.getKeys());
                    Record record = new Record();
                    for (int i = 0; i < data.length; i++)
                        record.set(data[i]);
                    rc = lc.createRecord(lgvv.getKeys(), record);
                } else {
                    Record record = DataListUtils.getRecord(cc, lc,
                            lgvv.getKeys());
                    rc = lc.createRecord(lgvv.getKeys(), record);
                    rc.setParameter("814", 'R');
                    rc.setParameter("815", Boolean.FALSE);
                }
            }
            return new SimpleResponseMessage(rc.getValue(gvv.getName()));
        } else
            return new SimpleResponseMessage(cc.getWorkflowContext()
                    .getRecord().getValue(gvv.getName()));
    }

}
