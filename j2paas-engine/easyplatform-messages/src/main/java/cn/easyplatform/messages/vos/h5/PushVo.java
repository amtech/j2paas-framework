/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.h5;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import cn.easyplatform.type.Constants;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PushVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -4959826813975186235L;

    private String id;

    private String code = "U";

    private int openModel = Constants.OPEN_MODAL;

    private List<PushFieldVo> fields;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getOpenModel() {
        return openModel;
    }

    public void setOpenModel(int openModel) {
        this.openModel = openModel;
    }

    public List<PushFieldVo> getFields() {
        return fields;
    }

    public void setFields(List<PushFieldVo> fields) {
        this.fields = fields;
    }

    public PushVo id(String id) {
        this.id = id;
        return this;
    }

    public PushVo code(String code) {
        this.code = code;
        return this;
    }

    public PushVo openModel(int openModel) {
        this.openModel = openModel;
        return this;
    }

    public PushVo field(String name, Object value) {
        if (this.fields == null)
            this.fields = new ArrayList<PushFieldVo>();
        this.fields.add(new PushFieldVo(name, value));
        return this;
    }

}
