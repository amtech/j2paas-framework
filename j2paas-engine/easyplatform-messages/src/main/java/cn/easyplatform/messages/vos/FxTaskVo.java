/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FxTaskVo extends TaskVo {

    private static final long serialVersionUID = -3118506048589325626L;

    private Serializable sessionId;

    private String workflowId;

    public FxTaskVo(String id, Serializable sessionId, String workflowId) {
        super(id);
        this.sessionId = sessionId;
        this.workflowId = workflowId;
    }

    public Serializable getSessionId() {
        return sessionId;
    }

    public String getWorkflowId() {
        return workflowId;
    }
}
