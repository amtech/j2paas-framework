/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.Queryable;
import cn.easyplatform.web.ext.Reloadable;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.ZkExt;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.ext.Disable;
import org.zkoss.zul.Grid;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Kalendars extends Grid implements ZkExt, Reloadable, Disable, Queryable {


    /**
     * 查询语句
     */
    private Object _query;

    /**
     * 数据连接的资源id
     */
    private String _dbId;

    /**
     * 列数
     */
    private int _cols = 4;

    /**
     * 过滤表达式
     */
    private String _filter;

    /**
     * 在显示时是否要马上执行查询
     */
    private boolean _immediate = true;

    /**
     * 是否可以触发事件
     */
    private boolean _disabled;
    /**
     * 周是哪一天开始
     */
    private int _firstDayOfWeek = 2;
    /**
     * 指定年份
     */
    private int _curYear;
    /**
     * 当前选择的月历事件
     */
    private transient Event _events;
    /**
     * 是否必需重新加载
     */
    private boolean _force;

    /**
     * 提示事件
     */
    private String _tooltipEvent;


    public String getTooltipEvent() {
        return _tooltipEvent;
    }

    public void setTooltipEvent(String tooltipEvent) {
        this._tooltipEvent = tooltipEvent;
    }

    @Override
    public boolean isForce() {
        return _force;
    }

    public void setForce(boolean force) {
        this._force = force;
    }
    public Event getEvents() {
        return _events;
    }

    public void setEvents(Event events) {
        this._events = events;
    }

    public void setCurrentYear(int value) {
        this._curYear = value;
    }

    public int getCurrentYear() {
        return this._curYear;
    }

    public void setFirstDayOfWeek(int value) {
        this._firstDayOfWeek = value;
    }

    public int getFirstDayOfWeek() {
        return _firstDayOfWeek;
    }

    public int getCols() {
        return _cols;
    }

    public void setCols(int cols) {
        this._cols = cols;
    }

    public String getFilter() {
        return _filter;
    }

    public void setFilter(String filter) {
        this._filter = filter;
    }

    public boolean isImmediate() {
        return _immediate;
    }

    public void setImmediate(boolean immediate) {
        this._immediate = immediate;
    }

    public Object getQuery() {
        return _query;
    }

    public void setQuery(Object query) {
        this._query = query;
    }

    public String getDbId() {
        return _dbId;
    }

    public void setDbId(String dbId) {
        this._dbId = dbId;
    }

    @Override
    public boolean isDisabled() {
        return _disabled;
    }

    @Override
    public void setDisabled(boolean disabled) {
        _disabled = disabled;
    }

    @Override
    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        ext.reload(this);
    }
}
