/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import cn.easyplatform.type.FieldVo;

import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PageVo extends AbstractPageVo implements VisibleVo {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // 宽
    private int width;
    // 高
    private int height;
    // 页面栏位信息
    private List<FieldVo> fields;
    // 页面内容
    private String page;
    // CRUD标志
    private String processCode;
    // 页面准备就绪后的事件
    private String onVisible;
    // 定义页面事件的方法
    private String script;
    // 如果openModel==OPEN_HIGHLIGHT||openModel==OPEN_MODAL,可以指定弹出窗口的位置
    private String position;
    // 页面是否可视
    private boolean isVisible;
    //要订阅消息的名称
    private String destination;
    //消息到达事件
    private String onMessage;
    //显示事件，在onVisible之前
    private String onShow;
    //绑定的变量
    private Map<String, Object> bindVariables;
    //页面权限
    private String[] access;
    //页面风格内容
    private String style;
    //页面脚本内容
    private String javascript;
    //自定义实现类
    private String apply;

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public String getJavascript() {
        return javascript;
    }

    public void setJavascript(String javascript) {
        this.javascript = javascript;
    }

    public String[] getAccess() {
        return access;
    }

    public void setAccess(String[] access) {
        this.access = access;
    }

    public String getOnShow() {
        return onShow;
    }

    public void setOnShow(String onShow) {
        this.onShow = onShow;
    }

    public Map<String, Object> getBindVariables() {
        return bindVariables;
    }

    public void setBindVariables(Map<String, Object> bindVariables) {
        this.bindVariables = bindVariables;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public List<FieldVo> getFields() {
        return fields;
    }

    public void setFields(List<FieldVo> fields) {
        this.fields = fields;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getOnVisible() {
        return onVisible;
    }

    public void setOnVisible(String onVisible) {
        this.onVisible = onVisible;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOnMessage() {
        return onMessage;
    }

    public void setOnMessage(String onMessage) {
        this.onMessage = onMessage;
    }

    /**
     * @return the isVisible
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * @param isVisible the isVisible to set
     */
    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public String getApply() {
        return apply;
    }

    public void setApply(String apply) {
        this.apply = apply;
    }
}
