{
    backgroundColor: {
        image: "(function(){var p = new Image();p.src=zk.ajaxURI('/web/js/echarts/img/bg.png', {au: true});return p})()",
        repeat: 'repeat'
    },
    title: {
        text: '饼图纹理',
        textStyle: {
            color: '#235894'
        }
    },
    tooltip: {},
    series: [{
        name: 'pie',
        type: 'pie',
        selectedMode: 'single',
        selectedOffset: 30,
        clockwise: true,
        label: {
            fontSize: 18,
            color: '#235894'
        },
        labelLine: {
            lineStyle: {
                color: '#235894'
            }
        },
        data: 'data',
        itemStyle: {
            normal: {
                opacity: 0.7,
                color: {
                    image: "(function(){var p = new Image();p.src=zk.ajaxURI('/web/js/echarts/img/pattern.jpg', {au: true});return p})()",
                    repeat: 'repeat'
                },
                borderWidth: 3,
                borderColor: '#235894'
            }
        }
    }]
}