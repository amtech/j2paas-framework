/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.engine.runtime.batch.BatchProcess;
import cn.easyplatform.engine.runtime.datalist.CatelogDataListProcess;
import cn.easyplatform.engine.runtime.datalist.DefaultSelectableDataListProcess;
import cn.easyplatform.engine.runtime.datalist.DetailDataListProcess;
import cn.easyplatform.engine.runtime.datalist.ReportDataListProcess;
import cn.easyplatform.engine.runtime.jasper.JasperReportProcess;
import cn.easyplatform.engine.runtime.jxls.JxlsReportProcess;
import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.SubType;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class ComponentProcessFactory {

    private ComponentProcessFactory() {
    }

    public static final DataListProcess createDatalistProcess(String type) {
        if (type.equalsIgnoreCase(Constants.CATALOG))
            return new CatelogDataListProcess();
        if (type.equalsIgnoreCase(Constants.DETAIL))
            return new DetailDataListProcess();
        if (type.equalsIgnoreCase(Constants.ACTIONBOX)
                || type.equalsIgnoreCase(Constants.LOADER))
            return new DefaultSelectableDataListProcess();
        if (type.equalsIgnoreCase(Constants.REPORT) || type.equalsIgnoreCase(Constants.HIERARCHY))
            return new ReportDataListProcess();
        throw new EasyPlatformWithLabelKeyException("datalist.type.not.found", type);
    }

    @SuppressWarnings("unchecked")
    public static final <E extends BaseEntity, V, T> ComponentProcess<E, V, T> createComponentProcess(
            BaseEntity base) {
        if (base.getType().equals(EntityType.REPORT.getName())) {
            if (SubType.JASPER.getName().equals(base.getSubType()))
                return (ComponentProcess<E, V, T>) new JasperReportProcess();
            if (SubType.JXLS.getName().equals(base.getSubType()))
                return (ComponentProcess<E, V, T>) new JxlsReportProcess();
            throw new EasyPlatformWithLabelKeyException("report.type.invalid",
                    base.getId(), base.getType());
        } else if (base.getType().equals(EntityType.BATCH.getName()))
            return (ComponentProcess<E, V, T>) new BatchProcess();
        throw new EasyPlatformWithLabelKeyException("report.type.invalid",
                base.getId(), base.getType());
    }
}
