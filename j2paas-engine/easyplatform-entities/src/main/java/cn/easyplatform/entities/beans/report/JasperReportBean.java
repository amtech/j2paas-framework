/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.report;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.helper.EventLogic;

import javax.xml.bind.annotation.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"dsId", "table", "query", "groupBy", "orderBy", "content",
        "serialize", "onRecord", "onGroup"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "report")
public class JasperReportBean extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 3111697139687952363L;

    @XmlElement
    private String dsId;

    @XmlElement
    private String table;
    /**
     * select a.x1,a.x2,a.name,a.level,a.xx,.... from A a left join B b where
     * xxx
     */
    @XmlElement
    private String query;

    /**
     * 排序语句
     */
    @XmlElement
    private String orderBy;

    /**
     * groupby
     */
    @XmlElement
    private String groupBy;

    @XmlElement
    private EventLogic onRecord;

    @XmlElement
    private EventLogic onGroup;

    @XmlElement(required = true)
    private byte[] content;

    @XmlElement
    private byte[] serialize;

    private Object reportModel;

    /**
     * @return the dsId
     */
    public String getDsId() {
        return dsId;
    }

    /**
     * @param dsId the dsId to set
     */
    public void setDsId(String dsId) {
        this.dsId = dsId;
    }

    /**
     * @return the query
     */
    public String getQuery() {
        return query;
    }

    /**
     * @param query the query to set
     */
    public void setQuery(String query) {
        this.query = query;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Object getReportModel() {
        return reportModel;
    }

    public void setReportModel(Object reportModel) {
        this.reportModel = reportModel;
    }

    public byte[] getSerialize() {
        return serialize;
    }

    public void setSerialize(byte[] serialize) {
        this.serialize = serialize;
    }

    public EventLogic getOnRecord() {
        return onRecord;
    }

    public void setOnRecord(EventLogic onRecord) {
        this.onRecord = onRecord;
    }

    public EventLogic getOnGroup() {
        return onGroup;
    }

    public void setOnGroup(EventLogic onGroup) {
        this.onGroup = onGroup;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the groupBy
     */
    public String getGroupBy() {
        return groupBy;
    }

    /**
     * @param groupBy the groupBy to set
     */
    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
