/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.project;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.demo.batch.BatchTest;
import cn.easyplatform.demo.bpm.BpmTest;
import cn.easyplatform.demo.datalist.DatalistTest;
import cn.easyplatform.demo.page.PageTest;
import cn.easyplatform.demo.report.ReportTest;
import cn.easyplatform.demo.resource.db.DataSourceTest;
import cn.easyplatform.demo.resource.job.JobTest;
import cn.easyplatform.demo.table.BizTableTest;
import cn.easyplatform.demo.table.SysTableTest;
import cn.easyplatform.demo.task.TaskTest;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.transaction.Atom;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ProjectTest extends AbstractDemoTest {

	/**
	 * 创建一个demo系统
	 * 
	 * @throws Exception
	 */
	@Test
	public void test1() throws Exception {
		System.setProperty("easyplatform.home", System.getProperty("user.dir"));
		connect();
		if (DaoUtils.getDbType(ds1.toString()) == DaoUtils.SYBASE) {
			runTest();
		} else
			JdbcTransactions.exec(new Atom() {

				@Override
				public void run() {
					runTest();
				}
			});
		close();
	}

	private void runTest() {
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = JdbcTransactions.getConnection(ds1);
			// 先清除
			/**log.debug("正在清除项目数据");
			pstmt = conn.prepareStatement("delete from granite_model_info");
			pstmt.executeUpdate();
			DaoUtils.closeQuietly(pstmt);
			// 再创建
			log.debug("正在新建demo项目");
			pstmt = conn
					.prepareStatement("insert into granite_model_info (modelId,name,desp,type,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?)");
			pstmt.setString(1, "demo");
			pstmt.setString(2, "Granite Demo System");
			pstmt.setString(3, "Granite Demo System V5");
			String project = Streams.readAndClose(Streams.utf8r(getClass()
					.getResourceAsStream("project.xml")));
			pstmt.setString(4, EntityType.PROJECT.getName());
			pstmt.setString(5, project);
			pstmt.setTimestamp(6, new Timestamp(System.currentTimeMillis()));
			pstmt.setString(7, "admin");
			pstmt.setString(8, "C");
			pstmt.execute();
			DaoUtils.closeQuietly(pstmt);

			log.debug("新建demo项目成功");*/
			//testDataSource();
			//testTable();
			//new PageTest().test();
			//new TaskTest().test();
			//new DatalistTest().test1();
			//new ReportTest().test();
			//new BpmTest().test1();
			//new BatchTest().test1();
			//new JobTest().test1();
			new ReportTest().test();
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new EasyPlatformRuntimeException("ProjectTest", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}

	private void testDataSource() {
		new DataSourceTest().test1();
	}

	protected void testTable() {
		SysTableTest t = new SysTableTest();
		t.test1();
		t.test2();

		BizTableTest bt = new BizTableTest();
		bt.test1();
		//bt.test2();
	}
}
