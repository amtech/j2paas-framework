/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.jasper;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.interceptor.CommandContext;
import net.sf.jasperreports.engine.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
abstract class AbstractReportHandler extends JRAbstractScriptlet {

	protected RecordContext ctx;

	protected Collection<ListContext> lists;

	protected CommandContext cc;

	protected JasperReportBean rb;

	AbstractReportHandler(CommandContext cc, RecordContext ctx,
			Collection<ListContext> lists, JasperReportBean rb) {
		this.cc = cc;
		this.rb = rb;
		this.ctx = ctx;
		this.lists = lists;
	}

	protected abstract JasperPrint doReport(JasperReport jr)
			throws JRException;

	protected Map<String, Object> createParameters(JRParameter[] parameters) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		for (JRParameter param : parameters) {
			if (param.getValueClass() == JasperReport.class
					&& !param.getName().equals("JASPER_REPORT")) {
				JasperReportBean e = cc.getEntity(param.getName());
				if (e == null)
					throw new EasyPlatformWithLabelKeyException(
							"report.child.not.found", param.getName());
				if (e.getReportModel() == null)
					throw new EasyPlatformWithLabelKeyException(
							"report.entity.no.compile", param.getName());
				JasperReport jr = (JasperReport) e.getReportModel();
				params.put(param.getName(), jr);
			} else if (!param.isSystemDefined()
					&& param.getValueClass() != JRDataSource.class)
				params.put(param.getName(), ctx.getValue(param.getName()));
		}
		params.put("REPORT_SCRIPTLET", this);
		return params;
	}

	@Override
	public void beforeReportInit() throws JRScriptletException {

	}

	@Override
	public void afterReportInit() throws JRScriptletException {
	}

	@Override
	public void beforePageInit() throws JRScriptletException {
	}

	@Override
	public void afterPageInit() throws JRScriptletException {
	}

	@Override
	public void beforeColumnInit() throws JRScriptletException {
	}

	@Override
	public void afterColumnInit() throws JRScriptletException {
	}

	@Override
	public void beforeGroupInit(String groupName) throws JRScriptletException {
	}

	@Override
	public void afterGroupInit(String groupName) throws JRScriptletException {
	}

	@Override
	public void beforeDetailEval() throws JRScriptletException {
	}

	@Override
	public void afterDetailEval() throws JRScriptletException {
	}
}
