/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.excel.imp;

import org.zkoss.util.CacheMap;
import org.zkoss.util.Pair;

import java.text.DateFormatSymbols;
import java.util.Locale;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FullMonthData extends CircularData {
	private FullMonthData(String[] data, int type, Locale locale) {
		super(data, type, locale);
	}
	private static final CacheMap<Pair<Locale,Integer>,FullMonthData> _monthData;
	static {
		_monthData = new CacheMap<Pair<Locale,Integer>,FullMonthData>(4);
		_monthData.setLifetime(24*60*60*1000);
	}
	public static FullMonthData getInstance(int type, Locale locale) {
		final Pair<Locale,Integer> key = new Pair<Locale,Integer>(locale, Integer.valueOf(type));
		FullMonthData value = (FullMonthData) _monthData.get(key);
		if (value == null) { //create and cache
			DateFormatSymbols symbols = DateFormatSymbols.getInstance(locale);
			if (symbols == null) {
				symbols = DateFormatSymbols.getInstance(Locale.US);
			}
			String[] month13 = symbols.getMonths();
			String[] month12 = new String[12];
			System.arraycopy(month13, 0, month12, 0, 12);
			value = new FullMonthData(month12, type, locale);
			_monthData.put(key, value);
		}
		return value;
	}
}
