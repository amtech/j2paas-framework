/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.task;

import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.engine.runtime.RuntimeTask;
import cn.easyplatform.engine.runtime.RuntimeTaskFactory;
import cn.easyplatform.entities.beans.page.PageBean;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.PageVo;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.DeviceType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.Collections;
import java.util.*;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PrevCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public PrevCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        WorkflowContext ctx = cc.getWorkflowContext();
        if (req.getBody() != null) {//手机端返回事件
            Map<String, Object> data = new HashMap<>();
            Object src = ctx.getParameter("838");
            if (src != null) {
                ctx.getRecord().setParameter("808", Constants.ON_BACK);
                String result = RuntimeUtils.eval(cc, src, ctx.getRecord());
                if (!result.equals("0000"))
                    return MessageUtils.byErrorCode(cc, ctx.getRecord(),
                            ctx.getId(), result);
                String[] fields = (String[]) req.getBody();
                for (String name : fields)
                    data.put(name, ctx.getRecord().getValue(name));

            }
            PageBean pb = cc.getEntity(ctx.getParameterAsString("831"));
            if (Strings.isBlank(pb.getMil())) {
                if (pb.getAjaxDevice() != null && pb.getAjaxDevice().getBindVariables() != null) {
                    PageVo pv = new PageVo();
                    RuntimeUtils.bindVariables(cc, ctx, pb.getAjaxDevice().getBindVariables(), pv);
                    data.putAll(pv.getBindVariables());
                }
            } else {
                if (pb.getMilDevice() != null && pb.getMilDevice().getBindVariables() != null) {
                    PageVo pv = new PageVo();
                    RuntimeUtils.bindVariables(cc, ctx, pb.getMilDevice().getBindVariables(), pv);
                    data.putAll(pv.getBindVariables());
                }
            }
            return new SimpleResponseMessage(data);
        } else {
            if (!ctx.prev(cc)) {
                cc.removeWorkflowContext();
                return new SimpleResponseMessage();
            }
            RuntimeTask support = RuntimeTaskFactory.createRuntime(ctx
                    .getParameterAsString("830"));
            return support.doPrev(cc);
        }
    }

    @Override
    public String getName() {
        return "task.Prev";
    }
}
