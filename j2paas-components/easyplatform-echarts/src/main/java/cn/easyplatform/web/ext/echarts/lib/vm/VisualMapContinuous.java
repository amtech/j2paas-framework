/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.vm;

import cn.easyplatform.web.ext.echarts.lib.type.VisualMapType;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class VisualMapContinuous extends VisualMap {
    /**
     * 指定手柄对应数值的位置。range 应在 min max 范围内
     */
    private Object range;
    /**
     * 拖拽时，是否实时更新
     */
    private Boolean realtime;
    /**
     * 是否显示拖拽用的手柄（手柄能拖拽调整选中范围）
     */
    private Boolean calculable;

    public VisualMapContinuous() {
        this.setType(VisualMapType.continuous.name());
    }

    public Boolean realtime() {
        return this.realtime;
    }

    public VisualMapContinuous realtime(Boolean realtime) {
        this.realtime = realtime;
        return this;
    }

    public Boolean calculable() {
        return this.calculable;
    }

    public VisualMapContinuous calculable(Boolean calculable) {
        this.calculable = calculable;
        return this;
    }

    public Object range() {
        return this.range;
    }

    public VisualMapContinuous range(Object... range) {
        if (range.length == 0 || range[0] == null)
            return this;
        this.range = range;
        return this;
    }
}
