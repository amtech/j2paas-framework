/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.service.impl;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TaskServiceImpl implements TaskService {

	private TaskService broker = EngineFactory.me().getEngineService(
			TaskService.class);

	@Override
	public IResponseMessage<?> begin(BeginRequestMessage req) {
		return broker.begin(req);
	}

	@Override
	public IResponseMessage<?> save(SaveRequestMessage req) {
		return broker.save(req);
	}

	@Override
	public IResponseMessage<?> refresh(RefreshRequestMessage req) {
		return broker.refresh(req);
	}

	@Override
	public IResponseMessage<?> next(NextRequestMessage req) {
		return broker.next(req);
	}

	@Override
	public IResponseMessage<?> prev(SimpleRequestMessage req) {
		return broker.prev(req);
	}

	@Override
	public IResponseMessage<?> close(SimpleRequestMessage req) {
		return broker.close(req);
	}

	@Override
	public IResponseMessage<?> getFields(GetFieldsRequestMessage req) {
		return broker.getFields(req);
	}

	@Override
	public IResponseMessage<?> reload(ReloadRequestMessage req) {
		return broker.reload(req);
	}

	@Override
	public IResponseMessage<?> getValue(GetValueRequestMessage req) {
		return broker.getValue(req);
	}

	@Override
	public IResponseMessage<?> setValue(SetValueRequestMessage req) {
		return broker.setValue(req);
	}

	@Override
	public IResponseMessage<?> getSource(SimpleRequestMessage req) {
		return broker.getSource(req);
	}

	@Override
	public IResponseMessage<?> action(ActionRequestMessage req) {
		return broker.action(req);
	}
	
	public IResponseMessage<?> batch(ListBatchRequestMessage req){
		return broker.batch(req);
	}

	@Override
	public IResponseMessage<?> subscribe(SimpleTextRequestMessage req) {
		return broker.subscribe(req);
	}

	@Override
	public IResponseMessage<?> unsubscribe(SimpleTextRequestMessage req) {
		return broker.unsubscribe(req);
	}
}
