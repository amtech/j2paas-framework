/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.axis;

import cn.easyplatform.web.ext.echarts.lib.type.AxisType;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TimeAxis extends Axis {

    public TimeAxis() {
        this.type(AxisType.time.toString());
    }
    /**
     * 轴的朝向，默认水平朝向，可以设置成 'vertical' 垂直朝向
     */
    private String orient;

    /**
     * 获取orient值
     */
    public String orient() {
        return this.orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public TimeAxis orient(String orient) {
        this.orient = orient;
        return this;
    }

    /**
     * 获取orient值
     */
    public String getOrient() {
        return orient;
    }

    /**
     * 设置orient值
     *
     * @param orient
     */
    public void setOrient(String orient) {
        this.orient = orient;
    }
}
