/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series.base;

import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;
/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LabelLine {
    private Boolean show;
    private Integer length;
    private Integer length2;
    private Boolean smooth;
    private LineStyle lineStyle;

    public Integer length() {
        return this.length;
    }

    public LabelLine length(Integer length) {
        this.length = length;
        return this;
    }

    public Integer length2() {
        return this.length2;
    }

    public LabelLine length2(Integer length2) {
        this.length2 = length2;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public LabelLine show(Boolean show) {
        this.show = show;
        return this;
    }

    public Boolean smooth() {
        return this.smooth;
    }

    public LabelLine smooth(Boolean smooth) {
        this.smooth = smooth;
        return this;
    }

    public LineStyle lineStyle() {
        if (this.lineStyle == null)
            this.lineStyle = new LineStyle();
        return this.lineStyle;
    }

    public LabelLine lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getLength2() {
        return length2;
    }

    public void setLength2(Integer length2) {
        this.length2 = length2;
    }

    public Boolean getSmooth() {
        return smooth;
    }

    public void setSmooth(Boolean smooth) {
        this.smooth = smooth;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }
}
