/*
 * create by 陈云亮(shiny_vc@163.com)
 */
kdez.Kindeditor = zk.$extends(zk.Widget, {
	_value : '',
	_skin : 'default',
	_langType : 'zh_CN',
	_urlType : '',
	_newlineTag : 'p',
	_ctrlKey:null,
	_noDisableItems : null,
	_items : null,

	_autoHeight : false,
	_disabled : false,
	_filterMode : false,
	_wellFormatMode : true,
	_designMode : true,
	_fullscreenMode : false,
	_useContextmenu : true,
	_allowPreviewEmoticons : false,
	_allowImageUpload : false,
	_allowFlashUpload : false,
	_allowMediaUpload : false,
	_allowFileUpload : false,
	_allowFileManager : false,
	_fullscreenShortcut : false,
	_fillDescAfterUploadImage : false,
	_allowImageRemote : false,

	_resizeType : 0,
	_minHeight : 1,
	_minWidth : 1,

	_editor : null,

	$define : {
		ctrlKey : function(v){
			this._ctrlKey = v;
			if (this._editor){
				var key = parseInt(this._ctrlKey);
				if(key > 0)
					this._editor.ctrl(this._editor.edit.doc, key, this._onCtrlKey);
				else
					this._editor.ctrl(this._editor.edit.doc, wgt._ctrlKey, this._onCtrlKey);				
			}
		},
		minHeight : function(v) {
			if (this._editor)
				this._editor.options.minHeight = v;
		},
		minWidth : function(v) {
			if (this._editor)
				this._editor.options.minWidth = v;
		},
		urlType : function(v) {
			if (this._editor)
				this._editor.options.urlType = v;
		},
		newlineTag : function(v) {
			if (this._editor)
				this._editor.options.newlineTag = v;
		},
		filterMode : function(v) {
			if (this._editor)
				this._editor.options.filterMode = v;
		},
		wellFormatMode : function(v) {
			if (this._editor)
				this._editor.options.wellFormatMode = v;
		},
		disabled : function(v) {
			if (this._editor)
				this._editor.readonly(v);
		},
		fullscreenMode : function(v) {
			if (this._editor)
				this._editor.fullscreen(v);
		},
		skin : function(v) {
			this._skin = v;
		},
		value : function(v) {
			if (this._editor)
				this._editor.html(v);
		},
		toolbar : function(v) {
			if (v == 'Basic') {
				this._items = [ 'fontname', 'fontsize', '|', 'forecolor',
						'hilitecolor', 'bold', 'italic', 'underline',
						'removeformat', '|', 'justifyleft', 'justifycenter',
						'justifyright', 'insertorderedlist',
						'insertunorderedlist', '|', 'emoticons', 'link' ]
			}
			if (this.desktop)
				this.rerender();
		},
		noDisableItems : _zkf = function() {
			if (this.desktop)
				this.rerender();
		},
		noDisableItems : _zkf,
		items : _zkf,
		langType : _zkf,
		autoHeight : _zkf,
		designMode : _zkf,
		useContextmenu : _zkf,
		allowPreviewEmoticons : _zkf,
		allowImageUpload : _zkf,
		allowFlashUpload : _zkf,
		allowMediaUpload : _zkf,
		allowFileUpload : _zkf,
		allowFileManager : _zkf,
		fullscreenShortcut : _zkf,
		fillDescAfterUploadImage : _zkf,
		allowImageRemote : _zkf,
		resizeType : _zkf,

		width : function(v) {
			if (!v || !this.$n())
				return;
			this._setSize(v, 'width');
		},
		height : function(v) {
			if (!v || !this.$n())
				return;
			this._setSize(v, 'height');
		}
	},
	setVflex : function(v) {
		if (v == 'min')
			v = false;
		if (this._editor)
			this.$super(kdez.Kindeditor, 'setVflex', v);
		else
			this._tmpVflex = v;
	},
	setHflex : function(v) {
		if (v == 'min')
			v = false;
		if (this._editor)
			this.$super(kdez.Kindeditor, 'setHflex', v);
		else
			this._tmpHflex = v;
	},
	setFlexSize_ : function(sz, ignoreMargins) {
		if (this._editor) {
			var n = this.$n(), zkn = zk(n);
			if (sz.height !== undefined) {
				if (sz.height == 'auto')
					n.style.height = '';
				else if (sz.height != '')
					this.setFlexSizeH_(n, zkn, sz.height - 6, ignoreMargins);
				else {
					n.style.height = this._height || '';
					if (this._height)
						this._setSize(this._height, 'height');
					else
						this._setSize('200px', 'height');
				}
			}
			if (sz.width !== undefined) {
				if (sz.width == 'auto')
					n.style.width = '';
				else if (sz.width != '')
					this.setFlexSizeW_(n, zkn, sz.width - 1, ignoreMargins);
				else {
					n.style.width = this._width || '';
					if (this._width)
						this._setSize(this._width, 'width');
					else
						this._setSize('100%', 'width');
				}
			}
			return {
				height : n.offsetHeight,
				width : n.offsetWidth
			};
		}
	},
	setFlexSizeH_ : function(n, zkn, height, ignoreMargins) {
		this._hflexHeight = height;
		this.$super(kdez.Kindeditor, 'setFlexSizeH_', n, zkn, height,
				ignoreMargins);
		var h = parseInt(n.style.height);
		n.style.height = '';
		this._setSize(jq.px0(h), 'height');
	},
	setFlexSizeW_ : function(n, zkn, width, ignoreMargins) {
		this.$super(kdez.Kindeditor, 'setFlexSizeW_', n, zkn, width,
				ignoreMargins);
		var w = parseInt(n.style.width);
		this._setSize(jq.px0(w), 'width');
	},
	_setSize : function(value, prop) {
		value = this._getValue(value);
		if (!value)
			return;
		if (prop == 'width')
			this._editor.resize(value, null);
		else
			this._editor.resize(null, value);
	},
	_getValue : function(value) {
		if (!value)
			return null;
		if (value.endsWith('%'))
			return zk.ie ? jq.px0(jq(this.$n()).width()) : value;
		return jq.px0(zk.parseInt(value));
	},
	redraw : function(out) {
		out.push('<div', this.domAttrs_({
			domStyle : true
		}), '><textarea id="', this.uuid, '-cnt"></textarea></div>');
	},
	domAttrs_ : function(no) {
		var attr = this.$supers('domAttrs_', arguments);
		if (!this.isVisible() && (!no || !no.visible))
			attr += ' style="display:none;"';
		return attr;
	},
	bind_ : function() {
		this.$supers('bind_', arguments);
		var cnt = this.$n('cnt');
		jq(cnt).text(this._value);
		var wgt = this;
		setTimeout(function() {
			wgt._init()
		}, 50);
	},
	unbind_ : function() {
		if (!this._editor) {
			this._unbind = true;
			this._arguments = arguments;
			return;
		}
		try {
			this._editor.remove();
			this._editor = null;
		} catch (e) {
		}
		this._items = null;
		this._noDisableItems = null;
		this._unbind = this._editor = this._tmpVflex = this._tmpHflex = null;
		this.$supers('unbind_', arguments);
	},
	focus : function() {
		if (this._editor)
			this._editor.focus();
	},
	blur : function() {
		if (this._editor)
			this._editor.blur();
	},
	_init : function() {
		var packagePath = zk.ajaxURI('/web/js/kdez/ext/', {
			desktop : this.desktop,
			au : true
		});
		var base = packagePath.substr(0, packagePath.lastIndexOf("/") + 1);
		var wgt = this;
		if (!this._items)
			this._items = KindEditor.options.items;
		else if (!KindEditor.isArray(this._items))
			this._items = this._items.split(',');
		if (!this._noDisableItems)
			this._noDisableItems = KindEditor.options.noDisableItems;
		else if (!KindEditor.isArray(this._noDisableItems))
			this._noDisableItems = this._noDisableItems.split(',');
		this._editor = KindEditor.create('#' + this.uuid + '-cnt', {
			width : this._width,
			height : this._height,
			items : this._items,
			minWidth : this._minWidth,
			minHeight : this._minHeight,
			noDisableItems : this._noDisableItems,
			wellFormatMode : this._wellFormatMode,
			designMode : this._designMode,
			fullscreenMode : this._fullscreenMode,
			urlType : this._urlType,
			newlineTag : this._newlineTag,
			useContextmenu : this._useContextmenu,
			allowPreviewEmoticons : this._allowPreviewEmoticons,
			allowImageUpload : this._allowImageUpload,
			allowFlashUpload : this._allowFlashUpload,
			allowMediaUpload : this._allowMediaUpload,
			allowFileUpload : this._allowFileUpload,
			allowFileManager : this._allowFileManager,
			fullscreenShortcut : this._fullscreenShortcut,
			fillDescAfterUploadImage : this._fillDescAfterUploadImage,
			allowImageRemote : this._allowImageRemote,
			resizeType : this._resizeType,
			langType : this._langType,
			themeType : this._skin,
			filterMode : this._filterMode,
			syncType : '',
			autoHeightMode : this._autoHeight,
			basePath : base,

			afterChange : function() {
				if (wgt._editor) {
					var value = wgt._editor.html();
					if (wgt._value != value) {
						wgt._value = value;
						if (wgt.isListen('onChanging'))
							wgt.fire("onChanging", {
								value : value
							}, {
								sendAhead : false
							});
					}
				}
			},
			afterBlur : function() {
				wgt.fire("onChange", {
					value : wgt._value
				}, {
					sendAhead : false
				});
			},
			afterCreate : function() {
				setTimeout(function() {
					if (wgt._resizeType == 0) {
						wgt._editor.statusbar.height(0);
						wgt._editor.statusbar.hide();
					}
					if (!wgt._tmpHflex && wgt._hflex) {
						wgt._tmpHflex = wgt._hflex;
						wgt.setHflex(null);
					}
					if (!wgt._tmpVflex && wgt._vflex) {
						wgt._tmpVflex = wgt._vflex;
						wgt.setVflex(null);
					}
					if (wgt._tmpHflex) {
						wgt.setHflex(wgt._tmpHflex, {
							force : true
						});
						wgt._tmpHflex = null;
					}
					if (wgt._tmpVflex) {
						wgt.setVflex(wgt._tmpVflex, {
							force : true
						});
						wgt._tmpVflex = null;
					}
				}, 150);
			}
		});
		if(this._ctrlKey){
			var onCtrlKey = function(){
				var value = wgt._editor.html();
				if (wgt.isListen('onCtrlKey'))
					wgt.fire("onCtrlKey", {
						value : value
					}, {
						sendAhead : false
					});				
			};
			var key = parseInt(this._ctrlKey);
			if(key > 0)
				KindEditor.ctrl(this._editor.edit.doc, key, onCtrlKey);
			else
				KindEditor.ctrl(this._editor.edit.doc, this._ctrlKey, onCtrlKey);
		}
	}
});