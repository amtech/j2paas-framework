/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.engine.runtime.ComponentProcessFactory;
import cn.easyplatform.engine.runtime.DataListProcess;
import cn.easyplatform.entities.beans.list.ListBean;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.ListInitRequestMessage;
import cn.easyplatform.messages.response.ListInitResponseMessage;
import cn.easyplatform.messages.vos.ListInitVo;
import cn.easyplatform.type.Constants;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class InitCmd extends AbstractCommand<ListInitRequestMessage> {

    /**
     * @param req
     */
    public InitCmd(ListInitRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        ListInitVo iv = req.getBody();
        if (Strings.isBlank(iv.getType()))
            iv.setType(Constants.CATALOG);
        if (cc.getWorkflowContext().getParameter("850") != null
                && (iv.getType().equals(Constants.CATALOG) || iv.getType()
                .equals(Constants.DETAIL)))
            return MessageUtils.dataListNotSupport(req.getBody().getId());
        ListBean bean = cc.getEntity(iv.getEntityId());
        if (bean == null)
            return MessageUtils.entityNotFound(EntityType.DATALIST.getName(),
                    iv.getEntityId());
        DataListProcess dlp = ComponentProcessFactory.createDatalistProcess(iv
                .getType());
        return new ListInitResponseMessage(dlp.init(cc, bean, iv));
    }

    @Override
    public String getName() {
        return "list.Init";
    }
}
