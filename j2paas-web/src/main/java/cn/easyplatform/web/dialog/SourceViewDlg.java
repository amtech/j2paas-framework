/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.vos.GetSourceVo;
import cn.easyplatform.web.ext.cmez.CMeditor;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Page;
import org.zkoss.zul.Window;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SourceViewDlg extends Window {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SourceViewDlg(GetSourceVo gsv, Page page) {
		this.setPage(page);
		createContent(gsv);
	}

	private void createContent(GetSourceVo gsv) {
		CMeditor view = new CMeditor();
		view.setVflex("1");
		view.setHflex("1");
		view.setMode(gsv.getType());
		view.setValue(gsv.getSource());
		view.setHeight(null);
		view.setFoldGutter(true);
		view.setLineNumbers(true);
		view.setDisabled(true);
		view.setParent(this);
		if (Strings.isBlank(gsv.getTitle()))
			setTitle(Labels.getLabel("app.source"));
		else
			setTitle(Labels.getLabel("app.source") + ":" + gsv.getTitle());
		setClosable(true);
		setMaximizable(true);
		setBorder(true);
		setSizable(true);
		setWidth("650px");
		setHeight("450px");
		this.setPosition("top,center");
		this.doOverlapped();
	}
}
