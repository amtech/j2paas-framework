/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages;

import cn.easyplatform.type.IResponseMessage;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public abstract class AbstractResponseMessage<T> implements
        IResponseMessage<T>, Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 6527422492297221624L;

    /**
     * 成功标志
     */
    public final static String SUCCESS = "0000";

    /**
     * 错误代码
     */
    protected String code = SUCCESS;

    /**
     * 信息载体
     */
    protected T body;

    /**
     * 是否重新开始
     */
    private boolean redo;
    /**
     * 当前运行功能的id
     *
     * @return
     */
    private String id;

    /**
     * @param body
     */
    public AbstractResponseMessage(T body) {
        this.body = body;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the body
     */
    public T getBody() {
        return body;
    }

    @Override
    public boolean isSuccess() {
        return code.equals(SUCCESS);
    }

    public boolean isRedo() {
        return redo;
    }

    public void setRedo(boolean redo) {
        this.redo = redo;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
