/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import java.io.Serializable;

public class AmbientCubemap implements Serializable {
    /**
     * 环境光贴图的 url，支持使用.hdr格式的 HDR 图片。可以从 http://www.hdrlabs.com/sibl/archive.html 等网站获取 .hdr 的资源。
     */
    private String texture;
    /**
     * 漫反射的强度。
     */
    private int diffuseIntensity;
    /**
     * 高光反射的强度。
     */
    private int specularIntensity;

    public String texture() {
        return this.texture;
    }
    public AmbientCubemap texture(String texture) {
        this.texture = texture;
        return this;
    }

    public Integer diffuseIntensity() {
        return this.diffuseIntensity;
    }
    public AmbientCubemap diffuseIntensity(Integer diffuseIntensity) {
        this.diffuseIntensity = diffuseIntensity;
        return this;
    }

    public Integer specularIntensity() {
        return this.specularIntensity;
    }
    public AmbientCubemap specularIntensity(Integer specularIntensity) {
        this.specularIntensity = specularIntensity;
        return this;
    }

    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public int getDiffuseIntensity() {
        return diffuseIntensity;
    }

    public void setDiffuseIntensity(int diffuseIntensity) {
        this.diffuseIntensity = diffuseIntensity;
    }

    public int getSpecularIntensity() {
        return specularIntensity;
    }

    public void setSpecularIntensity(int specularIntensity) {
        this.specularIntensity = specularIntensity;
    }
}
