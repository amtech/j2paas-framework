/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UploadVo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private int columnsRow = -1;

    private int firstRowNum;

    private int firstColNum;

    private int lastRowNum;

    private int lastColNum;

    private String target;

    private String logic;

    private String before;

    private String after;

    private String path;

    private String charset;

    private List<FileVo> files;

    private String filename;

    private boolean obfuscation;

    private String backup;

    private String sheetNames;

    private String format;

    private String[] thumbnails;

    //是否显示进度
    private boolean progress;
    //多线程处理，每线程可以处理的条数
    private int countOfThread;

    public int getCountOfThread() {
        return countOfThread;
    }

    public void setCountOfThread(int countOfThread) {
        this.countOfThread = countOfThread;
    }

    /**
     * @param target
     * @param data
     */
    public UploadVo(String target, List<FileVo> files) {
        this.target = target;
        this.files = files;
    }

    /**
     * @return the firstRowNum
     */
    public int getFirstRowNum() {
        return firstRowNum;
    }

    /**
     * @param firstRowNum the firstRowNum to set
     */
    public void setFirstRowNum(int firstRowNum) {
        this.firstRowNum = firstRowNum;
    }

    /**
     * @return the firstColNum
     */
    public int getFirstColNum() {
        return firstColNum;
    }

    /**
     * @param firstColNum the firstColNum to set
     */
    public void setFirstColNum(int firstColNum) {
        this.firstColNum = firstColNum;
    }

    /**
     * @return the lastRowNum
     */
    public int getLastRowNum() {
        return lastRowNum;
    }

    /**
     * @param lastRowNum the lastRowNum to set
     */
    public void setLastRowNum(int lastRowNum) {
        this.lastRowNum = lastRowNum;
    }

    /**
     * @return the lastColNum
     */
    public int getLastColNum() {
        return lastColNum;
    }

    /**
     * @param lastColNum the lastColNum to set
     */
    public void setLastColNum(int lastColNum) {
        this.lastColNum = lastColNum;
    }

    /**
     * @return the logic
     */
    public String getLogic() {
        return logic;
    }

    /**
     * @param logic the logic to set
     */
    public void setLogic(String logic) {
        this.logic = logic;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @return the files
     */
    public List<FileVo> getFiles() {
        return files;
    }

    /**
     * @return the charset
     */
    public String getCharset() {
        return charset;
    }

    /**
     * @param charset the charset to set
     */
    public void setCharset(String charset) {
        this.charset = charset;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return the obfuscation
     */
    public boolean isObfuscation() {
        return obfuscation;
    }

    /**
     * @param obfuscation the obfuscation to set
     */
    public void setObfuscation(boolean obfuscation) {
        this.obfuscation = obfuscation;
    }

    /**
     * @return the backup
     */
    public String getBackup() {
        return backup;
    }

    /**
     * @param backup the backup to set
     */
    public void setBackup(String backup) {
        this.backup = backup;
    }

    /**
     * @return the before
     */
    public String getBefore() {
        return before;
    }

    /**
     * @param before the before to set
     */
    public void setBefore(String before) {
        this.before = before;
    }

    /**
     * @return the after
     */
    public String getAfter() {
        return after;
    }

    /**
     * @param after the after to set
     */
    public void setAfter(String after) {
        this.after = after;
    }

    /**
     * @return
     */
    public String getSheetNames() {
        return sheetNames;
    }

    /**
     * @param sheetNames
     */
    public void setSheetNames(String sheetNames) {
        this.sheetNames = sheetNames;
    }

    /**
     * @return the columnsRow
     */
    public int getColumnsRow() {
        return columnsRow;
    }

    /**
     * @param columnsRow the columnsRow to set
     */
    public void setColumnsRow(int columnsRow) {
        this.columnsRow = columnsRow;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isProgress() {
        return progress;
    }

    public void setProgress(boolean progress) {
        this.progress = progress;
    }

    public String[] getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(String[] thumbnails) {
        this.thumbnails = thumbnails;
    }
}
