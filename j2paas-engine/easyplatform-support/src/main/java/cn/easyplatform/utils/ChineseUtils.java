/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.utils;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.utils.charset.ZhCode;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class ChineseUtils {

	/**
	 * 中文简体转繁体
	 * 
	 * @param value
	 * @return
	 */
	public final static String zh2Big5(String value) {
		ZhCode zhcode = new ZhCode();
		return zhcode.convertString(value, ZhCode.GB2312, ZhCode.BIG5);
	}

	/**
	 * 中文繁体转简体
	 * 
	 * @param value
	 * @return
	 */
	public final static String big52Zh(String value) {
		ZhCode zhcode = new ZhCode();
		return zhcode.convertString(value, ZhCode.BIG5, ZhCode.GB2312);
	}

	/**
	 * 判断是否是中文字符
	 * 
	 * @param str
	 * @return
	 */
	public final static boolean isChinese(String str) {
		if (str == null)
			return false;
		char[] cs = str.toCharArray();
		for (char c : cs) {
			boolean b = Strings.isChineseCharacter(c);
			if (b)
				return true;
		}
		return false;
	}

	/**
	 * 判断是否是全角字符串
	 * 
	 * @param str
	 * @return
	 */
	public final static boolean isFullWidthString(String str) {
		return Strings.isFullWidthString(str);
	}

	/**
	 * 转换为半角字符串
	 * 
	 * @param str
	 * @return
	 */
	public final static String toHalfWidthString(String str) {
		return Strings.toHalfWidthString(str);
	}

	/**
	 * 获取汉字串拼音首字母，英文字符不变
	 * 
	 * @param chinese
	 *            汉字串
	 * @return 汉语拼音首字母
	 */
	public final static String toFirstSpell(String chinese) {
		StringBuilder pybf = new StringBuilder();
		char[] arr = chinese.toCharArray();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > 128) {
				try {
					String[] _t = PinyinHelper.toHanyuPinyinStringArray(arr[i],
							defaultFormat);
					if (_t != null) {
						pybf.append(_t[0].charAt(0));
					}
				} catch (BadHanyuPinyinOutputFormatCombination e) {
				}
			} else {
				pybf.append(arr[i]);
			}
		}
		return pybf.toString().replaceAll("\\W", "").trim();
	}

	/**
	 * 获取汉字串拼音，英文字符不变
	 * 
	 * @param chinese
	 *            汉字串
	 * @return 汉语拼音
	 */
	public final static String toFullSpell(String chinese) {
		StringBuilder pybf = new StringBuilder();
		char[] arr = chinese.toCharArray();
		HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
		defaultFormat.setCaseType(HanyuPinyinCaseType.LOWERCASE);
		defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > 128) {
				try {
					pybf.append(PinyinHelper.toHanyuPinyinStringArray(arr[i],
							defaultFormat)[0]);
				} catch (BadHanyuPinyinOutputFormatCombination e) {
					e.printStackTrace();
				}
			} else {
				pybf.append(arr[i]);
			}
		}
		return pybf.toString();
	}
}
