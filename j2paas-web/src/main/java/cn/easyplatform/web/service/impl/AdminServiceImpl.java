/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.service.impl;

import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.admin.ResourceSaveRequestMessage;
import cn.easyplatform.messages.request.admin.ServiceRequestMessage;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.service.AdminService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class AdminServiceImpl implements AdminService {

    private AdminService broker = EngineFactory.me().getEngineService(
            AdminService.class);

    @Override
    public IResponseMessage<?> getHome(SimpleRequestMessage req) {
        return broker.getHome(req);
    }

    @Override
    public IResponseMessage<?> getServices(SimpleRequestMessage req) {
        return broker.getServices(req);
    }

    @Override
    public IResponseMessage<?> getService(ServiceRequestMessage req) {
        return broker.getService(req);
    }

    @Override
    public IResponseMessage<?> setServiceState(ServiceRequestMessage req) {
        return broker.setServiceState(req);
    }

    @Override
    public IResponseMessage<?> getModel(SimpleRequestMessage req) {
        return broker.getModel(req);
    }

    @Override
    public IResponseMessage<?> getData(SimpleRequestMessage req) {
        return broker.getData(req);
    }

    @Override
    public IResponseMessage<?> getConfig(SimpleRequestMessage req) {
        return broker.getConfig(req);
    }

    @Override
    public IResponseMessage<?> initCache(SimpleRequestMessage req) {
        return broker.initCache(req);
    }

    @Override
    public IResponseMessage<?> enableCache(SimpleRequestMessage req) {
        return broker.enableCache(req);
    }

    @Override
    public IResponseMessage<?> getCacheList(SimpleRequestMessage req) {
        return broker.getCacheList(req);
    }

    @Override
    public IResponseMessage<?> resetCache(SimpleRequestMessage req) {
        return broker.resetCache(req);
    }

    @Override
    public IResponseMessage<?> reloadCache(SimpleRequestMessage req) {
        return broker.reloadCache(req);
    }

    @Override
    public IResponseMessage<?> deleteCache(SimpleRequestMessage req) {
        return broker.deleteCache(req);
    }

    @Override
    public IResponseMessage<?> saveProject(SimpleRequestMessage req) {
        return broker.saveProject(req);
    }

    @Override
    public IResponseMessage<?> saveResource(ResourceSaveRequestMessage req) {
        return broker.saveResource(req);
    }

    @Override
    public IResponseMessage<?> getLog(SimpleRequestMessage req) {
        return broker.getLog(req);
    }

    @Override
    public IResponseMessage<?> console(SimpleRequestMessage req) {
        return broker.console(req);
    }

    @Override
    public IResponseMessage<?> table(SimpleRequestMessage req) {
        return broker.table(req);
    }
}
