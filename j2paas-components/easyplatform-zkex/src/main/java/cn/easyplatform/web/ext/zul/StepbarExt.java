/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.*;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zkmax.zul.Step;
import org.zkoss.zkmax.zul.Stepbar;
import org.zkoss.zul.Timer;

import java.util.List;
import java.util.Objects;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StepbarExt extends Stepbar implements ZkExt, Reloadable, Queryable, Assignable {

    /**
     *
     */
    private static final long serialVersionUID = 8472104295591158010L;

    /**
     * 查询语句，可以有多个栏位
     */
    private Object _query;

    /**
     * 数据连接的资源id
     */
    private String _dbId;

    /**
     * 过滤表达式
     */
    private String _filter;

    /**
     * 是否必需重新加载
     */
    private boolean _force;

    /**
     * 在显示时是否要马上执行查询
     */
    private boolean _immediate = true;

    public Object getQuery() {
        return _query;
    }

    public void setQuery(Object query) {
        this._query = query;
    }

    public String getDbId() {
        return _dbId;
    }

    public void setDbId(String dbId) {
        this._dbId = dbId;
    }

    public String getFilter() {
        return _filter;
    }

    public void setFilter(String filter) {
        this._filter = filter;
    }

    public boolean isImmediate() {
        return _immediate;
    }

    public void setImmediate(boolean immediate) {
        this._immediate = immediate;
    }

    @Override
    public void setValue(Object value) {
        //不能直接调用setActiveStep，zk bug :UI can't be modified in the rendering phase
        final Timer timer = new Timer();
        timer.setDelay(10);
        timer.setRunning(true);
        getPage().getFirstRoot().appendChild(timer);
        timer.addEventListener(Events.ON_TIMER, event -> {
            List<Step> steps = getSteps();
            for (Step step : steps) {
                if (Objects.equals(value, step.getAttribute("value"))) {
                    setActiveStep(step);
                    break;
                }
            }
            timer.detach();
        });
    }

    @Override
    public Object getValue() {
        Step step = getActiveStep();
        if (step == null)
            return null;
        return step.getAttribute("value");
    }

    @Override
    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        if (ext != null)
            ext.reload(this);
    }

    @Override
    public boolean isForce() {
        return _force;
    }

    public void setForce(boolean force) {
        this._force = force;
    }
}
