/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.threeD;

import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;

import java.io.Serializable;

public class AxisLine implements Serializable {
    /**
     * 是否显示坐标轴轴线。
     */
    private boolean show;
    /**
     * 坐标轴刻度标签的显示间隔，在类目轴中有效。
     *
     * 默认会自动计算interval以保证较好的展示效果。
     *
     * 可以设置成 0 强制显示所有标签。
     *
     * 如果设置为 1，表示『隔一个标签显示一个标签』，如果值为 2，表示『隔两个标签显示一个标签』，以此类推。
     */
    private Object interval;
    /**
     * {color: '#48b', width: 2, type: 'solid'}
     *
     * @see LineStyle
     */
    private LineStyle lineStyle;

    public boolean show() {
        return this.show;
    }

    public AxisLine show(boolean show) {
        this.show = show;
        return this;
    }

    public Object interval() {
        return this.interval;
    }

    public AxisLine interval(Object interval) {
        this.interval = interval;
        return this;
    }

    public LineStyle lineStyle() {
        return this.lineStyle;
    }

    public AxisLine show(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public Object getInterval() {
        return interval;
    }

    public void setInterval(Object interval) {
        this.interval = interval;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }
}
