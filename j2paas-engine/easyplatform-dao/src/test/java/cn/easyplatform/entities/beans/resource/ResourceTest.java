/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.resource;

import cn.easyplatform.entities.EntityInfo;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.type.EntityType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URL;
import java.util.Date;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ResourceTest {

	public void test1() {

		EntityInfo bean = new EntityInfo();
		bean.setId("00001");
		bean.setName("业务数据库定义");
		bean.setDescription("基于derby的数据库连接池");
		bean.setType(EntityType.RESOURCE.getName());
		bean.setCreateDate(new Date());
		bean.setCreateUser("admin");
		bean.setStatus('C');

		URL url = getClass().getResource(
				"/cn/easyplatform/entities/beans/resource/1.properties");
		File file = new File(url.getFile());
		try {
			bean.setContent(Streams.readAndClose(new FileReader(file)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
}
