/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Animation;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PictorialBarData extends Animation implements Serializable {
    private String name;
    private Object value;
    private Object symbol;
    private Object symbolSize;
    private Double symbolRotate;
    private Object symbolOffset;
    private Object symbolPosition;
    private Object symbolRepeat;
    private String symbolRepeatDirection;
    private Object symbolMargin;
    private Boolean symbolClip;
    private Object symbolBoundingData;
    private Object symbolPatternSize;
    private Integer z;
    private ItemStyle label;
    private ItemStyle itemStyle;

    public PictorialBarData(Object value) {
        this.value = value;
    }

    public PictorialBarData(Object value, Object symbol) {
        this.value = value;
        this.symbol = symbol;
    }

    public Object symbolPosition() {
        return this.symbolPosition;
    }

    public PictorialBarData symbolPosition(Object symbolPosition) {
        this.symbolPosition = symbolPosition;
        return this;
    }

    public Object symbolRepeat() {
        return this.symbolRepeat;
    }

    public PictorialBarData symbolRepeat(Object symbolRepeat) {
        this.symbolRepeat = symbolRepeat;
        return this;
    }

    public String symbolRepeatDirection() {
        return this.symbolRepeatDirection;
    }

    public PictorialBarData symbolRepeatDirection(String symbolRepeatDirection) {
        this.symbolRepeatDirection = symbolRepeatDirection;
        return this;
    }

    public Object symbolMargin() {
        return this.symbolMargin;
    }

    public PictorialBarData symbolMargin(Object symbolMargin) {
        this.symbolMargin = symbolMargin;
        return this;
    }

    public Object symbolClip() {
        return this.symbolClip;
    }

    public PictorialBarData symbolClip(Boolean symbolClip) {
        this.symbolClip = symbolClip;
        return this;
    }

    public Object symbolBoundingData() {
        return this.symbolBoundingData;
    }

    public PictorialBarData symbolBoundingData(Object symbolBoundingData) {
        this.symbolBoundingData = symbolBoundingData;
        return this;
    }

    public Object symbolPatternSize() {
        return this.symbolPatternSize;
    }

    public PictorialBarData symbolPatternSize(Object symbolPatternSize) {
        this.symbolPatternSize = symbolPatternSize;
        return this;
    }

    public Integer z() {
        return this.z;
    }

    public PictorialBarData z(Integer z) {
        this.z = z;
        return this;
    }

    public String name() {
        return this.name;
    }

    public PictorialBarData name(String name) {
        this.name = name;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public PictorialBarData value(Object value) {
        this.value = value;
        return this;
    }

    public Object symbol() {
        return this.symbol;
    }

    public PictorialBarData symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return this.symbolSize;
    }

    public PictorialBarData symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Double symbolRotate() {
        return this.symbolRotate;
    }

    public PictorialBarData symbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return this.symbolOffset;
    }

    public PictorialBarData symbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public ItemStyle label() {
        if (label == null)
            label = new ItemStyle();
        return this.label;
    }

    public PictorialBarData label(ItemStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return this.itemStyle;
    }

    public PictorialBarData itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Double getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Double symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public Object getSymbolPosition() {
        return symbolPosition;
    }

    public void setSymbolPosition(Object symbolPosition) {
        this.symbolPosition = symbolPosition;
    }

    public Object getSymbolRepeat() {
        return symbolRepeat;
    }

    public void setSymbolRepeat(Object symbolRepeat) {
        this.symbolRepeat = symbolRepeat;
    }

    public String getSymbolRepeatDirection() {
        return symbolRepeatDirection;
    }

    public void setSymbolRepeatDirection(String symbolRepeatDirection) {
        this.symbolRepeatDirection = symbolRepeatDirection;
    }

    public Object getSymbolMargin() {
        return symbolMargin;
    }

    public void setSymbolMargin(Object symbolMargin) {
        this.symbolMargin = symbolMargin;
    }

    public Boolean getSymbolClip() {
        return symbolClip;
    }

    public void setSymbolClip(Boolean symbolClip) {
        this.symbolClip = symbolClip;
    }

    public Object getSymbolBoundingData() {
        return symbolBoundingData;
    }

    public void setSymbolBoundingData(Object symbolBoundingData) {
        this.symbolBoundingData = symbolBoundingData;
    }

    public Object getSymbolPatternSize() {
        return symbolPatternSize;
    }

    public void setSymbolPatternSize(Object symbolPatternSize) {
        this.symbolPatternSize = symbolPatternSize;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public ItemStyle getLabel() {
        return label;
    }

    public void setLabel(ItemStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }
}
