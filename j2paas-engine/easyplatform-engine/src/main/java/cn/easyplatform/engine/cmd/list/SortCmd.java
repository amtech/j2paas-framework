/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.engine.runtime.ComponentProcessFactory;
import cn.easyplatform.engine.runtime.DataListProcess;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.ListSortRequestMessage;
import cn.easyplatform.messages.response.ListPagingResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListPagingVo;
import cn.easyplatform.messages.vos.datalist.ListSortVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SortCmd extends AbstractCommand<ListSortRequestMessage> {

	/**
	 * @param req
	 */
	public SortCmd(ListSortRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		ListSortVo rv = req.getBody();
		ListContext lc = cc.getWorkflowContext().getList(rv.getId());
		if (lc == null)
			return MessageUtils.dataListNotFound(rv.getId());
		if (rv.isAscending())
			lc.setOrderBy(rv.getField() + " asc");
		else
			lc.setOrderBy(rv.getField() + " desc");
		DataListProcess dlp = ComponentProcessFactory.createDatalistProcess(lc
				.getType());
		return new ListPagingResponseMessage(dlp.paging(cc,
				new ListPagingVo(rv.getId(), 0)));
	}

	@Override
	public String getName() {
		return "list.Sort";
	}
}
