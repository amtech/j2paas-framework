/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.web.ext.ZkExt;
import org.zkoss.zul.Button;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Import extends Button implements ZkExt {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * button,link,drop
     */
    private String _type = "button";

    /**
     * 最大的上传文件大小,单位kb,-1表示没有限制
     */
    private int _maxSize = -1;

    /**
     * 是否可以多个文件上传
     */
    private boolean _multiple;

    /**
     * 能够被提交或上传的一个或多个 MIME 类型，如需规定多个 MIME
     * 类型，请使用逗号分隔这些类型,例如：audio/*|video/*|image
     */
    private String _accept;

    /**
     * 锚点，仅用在type为drop上面
     */
    private String _anchor;

    /**
     * 从第几行开始
     */
    private int _firstRowNum = 0;

    /**
     * 从第几列开始
     */
    private int _firstColNum = 0;

    /**
     * 到第几行结束
     */
    private int _lastRowNum = 0;

    /**
     * 到第几列结束
     */
    private int _lastColNum = 0;

    /**
     * 目标：[list|field]:name
     */
    private String _target = "";

    /**
     * 保存文件名的栏位
     */
    private String _filename;

    /**
     * 保存路径的栏位
     */
    private String _path;

    /**
     * 保存的路径
     */
    private String _backup;

    /**
     * 映射
     */
    private String _logic;

    /**
     * 前置逻辑
     */
    private String _before;

    /**
     * 后置逻辑
     */
    private String _after;

    /**
     * 字符集
     */
    private String _charset = "utf8";

    /**
     * 当target=path时文件名是否混淆
     */
    private boolean _obfuscation;

    /**
     * 当导入excel时要导入的sheet范围
     * 没赋值只导入第一,*表示导入所有sheet，如果是int类型表示要导入第几个sheet，可以有多个，用逗号分隔,如是不是数字，匹配对应的sheet名称
     */
    private String _sheetNames;

    /**
     * 如果columnsRow大于等于0，表示导入的栏位名称使用指定行的名称
     */
    private int columnsRow = -1;

    /**
     * 上传文件的内容格式
     */
    private String _format;

    /**
     * 5组缩略图
     */
    private String _thumbnail0;

    private String _thumbnail1;

    private String _thumbnail2;

    private String _thumbnail3;

    private String _thumbnail4;

    //是否显示进度
    private boolean _progress;

    /**
     * 多线程处理，每线程可以处理的条数
     */
    private int _countOfThread;

    public boolean isProgress() {
        return _progress;
    }

    public void setProgress(boolean progress) {
        this._progress = progress;
    }

    public int getCountOfThread() {
        return _countOfThread;
    }

    public void setCountOfThread(int countOfThread) {
        this._countOfThread = countOfThread;
    }

    public String getThumbnail0() {
        return _thumbnail0;
    }

    public void setThumbnail0(String thumbnail0) {
        this._thumbnail0 = thumbnail0;
    }

    public String getThumbnail1() {
        return _thumbnail1;
    }

    public void setThumbnail1(String thumbnail1) {
        this._thumbnail1 = thumbnail1;
    }

    public String getThumbnail2() {
        return _thumbnail2;
    }

    public void setThumbnail2(String thumbnail2) {
        this._thumbnail2 = thumbnail2;
    }

    public String getThumbnail3() {
        return _thumbnail3;
    }

    public void setThumbnail3(String thumbnail3) {
        this._thumbnail3 = thumbnail3;
    }

    public String getThumbnail4() {
        return _thumbnail4;
    }

    public void setThumbnail4(String thumbnail4) {
        this._thumbnail4 = thumbnail4;
    }

    public String getFormat() {
        return _format;
    }

    public void setFormat(String format) {
        this._format = format;
    }

    /**
     * @return the columnsRow
     */
    public int getColumnsRow() {
        return columnsRow;
    }

    /**
     * @param columnsRow the columnsRow to set
     */
    public void setColumnsRow(int columnsRow) {
        this.columnsRow = columnsRow;
    }

    /**
     * @return
     */
    public String getSheetNames() {
        return _sheetNames;
    }

    /**
     * @param sheetNames
     */
    public void setSheetNames(String sheetNames) {
        this._sheetNames = sheetNames;
    }

    /**
     * @return the obfuscation
     */
    public boolean isObfuscation() {
        return _obfuscation;
    }

    /**
     * @param obfuscation the obfuscation to set
     */
    public void setObfuscation(boolean obfuscation) {
        this._obfuscation = obfuscation;
    }

    /**
     * @return the charset
     */
    public String getCharset() {
        return _charset;
    }

    /**
     * @param charset the charset to set
     */
    public void setCharset(String charset) {
        this._charset = charset;
    }

    /**
     * @return the startRow
     */
    public int getFirstRowNum() {
        return _firstRowNum;
    }

    /**
     * @param firstRowNum the startRow to set
     */
    public void setFirstRowNum(int firstRowNum) {
        this._firstRowNum = firstRowNum;
    }

    /**
     * @return the startCol
     */
    public int getFirstColNum() {
        return _firstColNum;
    }

    /**
     * @param firstColNum the startCol to set
     */
    public void setFirstColNum(int firstColNum) {
        this._firstColNum = firstColNum;
    }

    /**
     * @return the _lastRowNum
     */
    public int getLastRowNum() {
        return _lastRowNum;
    }

    /**
     * @param lastRowNum the _lastRowNum to set
     */
    public void setLastRowNum(int lastRowNum) {
        this._lastRowNum = lastRowNum;
    }

    /**
     * @return the _lastColNum
     */
    public int getLastColNum() {
        return _lastColNum;
    }

    /**
     * @param lastColNum the _lastColNum to set
     */
    public void setLastColNum(int lastColNum) {
        this._lastColNum = lastColNum;
    }

    /**
     * @return the path
     */
    public String getPath() {
        return _path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this._path = path;
    }

    /**
     * @return the logic
     */
    public String getLogic() {
        return _logic;
    }

    /**
     * @param logic the logic to set
     */
    public void setLogic(String logic) {
        this._logic = logic;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return _target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this._target = target;
    }

    /**
     * @return the type
     */
    public String getType() {
        return _type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this._type = type;
    }

    /**
     * @return the maxSize
     */
    public int getMaxSize() {
        return _maxSize;
    }

    /**
     * @param maxSize the maxSize to set
     */
    public void setMaxSize(int maxSize) {
        this._maxSize = maxSize;
    }

    /**
     * @return the multiple
     */
    public boolean isMultiple() {
        return _multiple;
    }

    /**
     * @param multiple the multiple to set
     */
    public void setMultiple(boolean multiple) {
        this._multiple = multiple;
    }

    /**
     * @return the accept
     */
    public String getAccept() {
        return _accept;
    }

    /**
     * @param accept the accept to set
     */
    public void setAccept(String accept) {
        this._accept = accept;
    }

    /**
     * @return the anchor
     */
    public String getAnchor() {
        return _anchor;
    }

    /**
     * @param anchor the anchor to set
     */
    public void setAnchor(String anchor) {
        this._anchor = anchor;
    }

    /**
     * @return the filename
     */
    public String getFilename() {
        return _filename;
    }

    /**
     * @param filename the filename to set
     */
    public void setFilename(String filename) {
        this._filename = filename;
    }

    /**
     * @return the backup
     */
    public String getBackup() {
        return _backup;
    }

    /**
     * @param backup the backup to set
     */
    public void setBackup(String backup) {
        this._backup = backup;
    }

    /**
     * @return the before
     */
    public String getBefore() {
        return _before;
    }

    /**
     * @param before the before to set
     */
    public void setBefore(String before) {
        this._before = before;
    }

    /**
     * @return the after
     */
    public String getAfter() {
        return _after;
    }

    /**
     * @param after the after to set
     */
    public void setAfter(String after) {
        this._after = after;
    }

}
