/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.graphic;

import cn.easyplatform.web.ext.echarts.builder.support.GroupTypeAdapter;
import com.google.gson.annotations.JsonAdapter;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Group extends Graphic {
    /**
     * 用于描述此 group 的宽
     */
    private Object width;
    /**
     * 用于描述此 group 的高。
     */
    private Object height;
    /**
     * 在 自定义系列 中，当 diffChildrenByName: true 时，对于 renderItem 返回值中的每一个 group，会根据其 children 中每个图形元素的 name 属性进行 "diff"。在这里，"diff" 的意思是，重绘的时候，在已存在的图形元素和新的图形元素之间建立对应关系（依据 name 是否相同），从如果数据有更新，能够形成的过渡动画。
     * <p>
     * 但是注意，这会有性能开销。如果数据量较大，不要开启这个功能
     */
    private Boolean diffChildrenByName;

    /**
     * 子节点列表，其中项都是一个图形元素定义
     */
    @JsonAdapter(GroupTypeAdapter.class)
    private Graphic[] children;

    public Group() {
        this.type("group");
    }

    public Object width() {
        return width;
    }

    public Group width(Object width) {
        this.width = width;
        return this;
    }

    public Object height() {
        return height;
    }

    public Group height(Object height) {
        this.height = height;
        return this;
    }

    public Graphic[] children() {
        return children;
    }

    public Group children(Graphic... children) {
        this.children = children;
        return this;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Graphic[] getChildren() {
        return children;
    }

    public void setChildren(Graphic[] children) {
        this.children = children;
    }
}
