/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ocpsoft.prettytime;

/**
 * Represents a quantity of any given {@link TimeUnit}
 * 
 * @author <a href="mailto:lincolnbaxter@gmail.com">Lincoln Baxter, III</a>
 * 
 */
public interface Duration
{
   /**
    * Return the calculated quantity of {@link TimeUnit} instances.
    */
   public long getQuantity();

   /**
    * Return the calculated quantity of {@link TimeUnit} instances, rounded up if {@link #getDelta()} is greater than or
    * equal to the given tolerance.
    */
   public long getQuantityRounded(int tolerance);

   /**
    * Return the {@link TimeUnit} represented by this {@link Duration}
    */
   public TimeUnit getUnit();

   /**
    * Return the number of milliseconds left over when calculating the number of {@link TimeUnit}'s that fit into the
    * given time range.
    */
   public long getDelta();

   /**
    * Return true if this {@link Duration} represents a number of {@link TimeUnit} instances in the past.
    */
   public boolean isInPast();

   /**
    * Return true if this {@link Duration} represents a number of {@link TimeUnit} instances in the future.
    */
   public boolean isInFuture();
}
